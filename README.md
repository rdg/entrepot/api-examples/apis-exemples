# Exemples APIS

Ce dépôt contient des ressources exemples d'utilisation pour les APIs Recherche Data Gouv.

La documentation complète de ces APIs est accessible dans les [guides de Dataverse](https://guides.dataverse.org/en/5.12.1/api/index.html).

## Création d'un jeu de données

le dossier **dataset-creation-metadata** contient les fichiers de métadonnées exemples à utiliser pour créer un jeu de données via API.

Rappel : la commande de création à utiliser est la suivante :
```
curl -H "X-Dataverse-key:$API_TOKEN" -X POST $SERVER_URL/api/dataverses/tp/datasets" --upload-file rechercheDataGouv-minimum-metadata.json -H "Content-type:application/json"
```
